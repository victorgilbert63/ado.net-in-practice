﻿using WhatsAppDAL;
using WhatsAppDAL.Model;


namespace WhatsAppClient
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello, World!");

            using (IWhatsAppService whatsAppService = new WhatsAppService(new WhatsAppDbContext()) )
            {
                var userData = new UserViewModel
                {
                    Name = "Chizoba",
                    Email = "stephenlove@outlook.com",
                    PhoneNumber = "09095002354",
                    ProfilePhoto = "StevLove.png"
                };

             var createdUserId =  await whatsAppService.CreateUser(userData);

             Console.WriteLine(createdUserId);

            }
        }
    }
}